package com.sunlee.template.demo2;

public class ConcreteClass1 extends AbstractClass {
    //实现基本方法
    @Override
    protected void doAnything() {
        //业务逻辑处理
    }

    @Override
    protected void doSomething() {
        //业务逻辑处理
    }
}