package com.sunlee.template.demo2;

/**
 * 基本方法(子类可改)--也叫基本操作，是由子类实现的方法，并且在模板方法中被调用。
 * 模板方法(抽象类固定,一般场景类调用)--可以有几个或一个，一般是一个具体的方法，也就是一个框架，实现对基本方法的调用，完成固定的逻辑。
 * 钩子方法--约束函数行为
 */
public abstract class AbstractClass {
    //基本方法
    protected abstract void doSomething();

    //基本方法
    protected abstract void doAnything();

    //模板方法
    public void templateMethod() {
        /*
         * 调用基本方法，完成相关的逻辑
         */
        this.doAnything();
        this.doSomething();
    }
}
