package com.sunlee.template.demo1;

/**
 * 基本方法(子类可改)--也叫基本操作，是由子类实现的方法，并且在模板方法中被调用。
 * 模板方法(抽象类固定)--可以有几个或一个，一般是一个具体的方法，也就是一个框架，实现对基本方法的调用，完成固定的逻辑。
 * 钩子方法--约束函数行为
 */
public abstract class HummerModel {

    // 发动车
    protected abstract void start();

    // 停车
    protected abstract void stop();

    // 响喇叭
    protected abstract void alarm();

    // 引擎
    protected abstract void engineBoom();

    // 钩子方法
    protected boolean isAlarm() {
        return true;// 默认喇叭会响
    }

    // 运行
    final public void run() {
        this.start();// 发动汽车
        this.engineBoom();// 引擎启动
        if (this.isAlarm()) {
            this.alarm();// 按喇叭
        }
        this.stop();// 到达目的地停车
    }
}