package com.sunlee.template.demo1;
 
/**
 * @author kungfu~peng
 * @data 2017年11月16日
 * @description
 */
public class HummerH1Model extends HummerModel {

    private boolean alarmFlag = true;// 默认车的喇叭响

    @Override
    protected void start() {
        System.out.println("H1悍马发动！");
    }
 
    @Override
    protected void stop() {
        System.out.println("H1悍马停止！");
    }
 
    @Override
    protected void alarm() {
        System.out.println("H1悍马鸣笛！");
    }
 
    @Override
    protected void engineBoom() {
        System.out.println("H1悍马引擎声音。。。");
    }

    // 配合钩子方法
    @Override
    protected boolean isAlarm() {
        return this.alarmFlag;
    }

    // 喇叭响不响，有客户来决定
    public void setAlarm(boolean isAlarm) {
        this.alarmFlag = isAlarm;
    }
}