package com.sunlee.template.demo1;

/**
 * @Author sunjulei
 * @Date 2021/10/18
 */
public class Client {
    public static void main(String[] args) {
        System.out.println("-------H1型号悍马--------");
        HummerH1Model h1 = new HummerH1Model();
        h1.setAlarm(true);
        h1.run();

        System.out.println("\n-------H2型号悍马--------");
        HummerH2Model h2 = new HummerH2Model();
        h2.run();
    }
}
