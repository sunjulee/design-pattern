package com.sunlee.template.demo1;


public class HummerH2Model extends HummerModel {
    @Override
    public void start() {
        System.out.println("H2悍马发动！");
    }

    @Override
    public void stop() {
        System.out.println("H2悍马停止！");
    }

    @Override
    public void alarm() {
        System.out.println("H2悍马鸣笛！");
    }

    @Override
    public void engineBoom() {
        System.out.println("H2悍马引擎声音。。。");
    }

    @Override
    protected boolean isAlarm() {
        return false;
    }
}