package com.sunlee.adapter.demo1;

/**
 * 对象适配器
 *
 * @author sunlee
 */
public class Adapter implements Target {

    private Adaptee adaptee;

    public Adapter(Adaptee adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public int output5v() {
        int i = adaptee.output220v();
        System.out.println(String.format("将 %dv 转为 5v", i));
        return 5;
    }
}
