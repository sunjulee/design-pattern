package com.sunlee.adapter.demo2;

import com.sunlee.adapter.demo1.Target;

/**
 * 类的适配器模式
 *
 * @author sunlee
 */
public class Adapter extends Adaptee implements Target {


    @Override
    public int output5v() {
        int i = output220v();
        System.out.println(String.format("将 %dv 转为 5v", i));
        return 5;
    }
}
