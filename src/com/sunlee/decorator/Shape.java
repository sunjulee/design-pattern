package com.sunlee.decorator;

public interface Shape {
   void draw();
}