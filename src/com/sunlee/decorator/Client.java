package com.sunlee.decorator;

import javafx.scene.shape.Circle;

public class Client {
   public static void main(String[] args) {
 
      Shape bShape = new BShape();
      ShapeDecorator aShapeD = new RedShapeDecorator(new AShape());
      ShapeDecorator bShapeD = new RedShapeDecorator(new BShape());


      bShape.draw();
      System.out.println("---");
      aShapeD.draw();
      System.out.println("---");
      bShapeD.draw();
   }
}