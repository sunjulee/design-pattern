package com.sunlee.decorator;

/**
 * @Author sunjulei
 * @Date 2021/10/22
 */
public class BShape implements Shape{
    @Override
    public void draw() {
        System.out.println("draw: BShape");
    }
}
