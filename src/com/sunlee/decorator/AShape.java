package com.sunlee.decorator;

/**
 * @Author sunjulei
 * @Date 2021/10/22
 */
public class AShape implements Shape{
    @Override
    public void draw() {
        System.out.println("draw: AShape");
    }
}
