package com.sunlee.decorator;

/**
 * 1、动态地给一个对象添加一些额外的职责。 就增加功能来说，装饰模式相比生成子类更为灵活(装饰模式是继承关系的一个替代方案)
 * 2、扩展性非常好，但过多的装饰会使得代码变复杂，因减少装饰类的数量
 */
public class RedShapeDecorator extends ShapeDecorator {

    public RedShapeDecorator(Shape decoratedShape) {
        super(decoratedShape);
    }

    @Override
    public void draw() {
        decoratedShape.draw();
        setRedBorder(decoratedShape);
    }

    private void setRedBorder(Shape decoratedShape) {
        System.out.println("Border Color: Red");
    }
}