package com.sunlee.builder.demo1;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class Test {
    public static void main(String[] args) {
        Director director = new Director();
        Product build = director.build(new Worker());
        System.out.println(build.toString());
    }
}
