package com.sunlee.builder.demo1;

/**
 * 抽象的建造者：方法
 * @Author sunjulei
 * @Date 2021/10/17
 */
public abstract class Builder {
    abstract void builderA();
    abstract void builderB();
    abstract void builderC();
    abstract void builderD();

    //完工 得到产品
    abstract Product getProduct();
}
