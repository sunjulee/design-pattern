package com.sunlee.builder.demo1;

/**
 * 指挥：核心。负责指挥构建一个工程，工程如何构建，由他决定
 *
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class Director {

    // 指挥工人按照顺序构建
    public Product build(Builder builder) {
        builder.builderA();
        builder.builderB();
        builder.builderC();
        builder.builderD();
        return builder.getProduct();
    }
}
