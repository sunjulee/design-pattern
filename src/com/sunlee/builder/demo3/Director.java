package com.sunlee.builder.demo3;

/**
 * 导演类，可多个
 *
 * @author sunlee
 */
public class Director {

    private Builder builder = new ConcreteProduct();

    public Product getAProduct() {
        builder.setPart();
        /*
         * 设置不同的零件，产生不同的产品
         */
        return builder.builderProduct();
    }
}
