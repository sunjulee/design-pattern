package com.sunlee.builder.demo2;


/**
 * 抽象的建造者：方法
 * @Author sunjulei
 * @Date 2021/10/17
 */
public abstract class Builder {

    abstract Builder builderA(String msg);//汉堡
    abstract Builder builderB(String msg);//可乐
    abstract Builder builderC(String msg);//薯条
    abstract Builder builderD(String msg);//甜点


    abstract Product getProduct();
}
