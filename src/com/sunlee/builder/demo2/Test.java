package com.sunlee.builder.demo2;

/**
 * 产品的建造和表示分离，实现了解耦
 * 适合产品具有较多的共同点，如果差异性大则不适合建造者模式；如果产品内部变化复杂也不合适
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class Test {
    public static void main(String[] args) {
        Worker worker = new Worker();
        Product product = worker
                .builderA("全家桶")
                .builderB("雪碧")
                .getProduct();

        System.out.println(product.toString());
    }
}
