package com.sunlee.proxy.demo1;

/**
 * @author sunlee
 */
public interface Subject {
    void request();
}
