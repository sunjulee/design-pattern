package com.sunlee.proxy.demo1;

/**
 * @author sunlee
 */
public class RealSubject implements Subject{
    @Override
    public void request() {
        System.out.println("业务逻辑处理");
    }
}
