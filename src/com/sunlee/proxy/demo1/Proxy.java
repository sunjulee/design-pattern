package com.sunlee.proxy.demo1;

/**
 * @author sunlee
 */
public class Proxy implements Subject {
    //要代理哪个实现类
    private Subject subject;

    //通过构造函数传递代理者
    public Proxy(Subject objects) {
        this.subject = objects;
    }

    //实现接口中定义的方法
    @Override
    public void request() {
        this.before();
        this.subject.request();
        this.after();
    }

    private void after() {

    }

    private void before() {
    }
}
