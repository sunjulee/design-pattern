package com.sunlee.proxy.demo2;

/**
 * @author sunlee
 */
public interface UserService {

    void add();
    void delete();
    void update();
    void query();
}
