package com.sunlee.proxy.demo2;

/**
 * @author sunlee
 */
public class UserServiceImpl implements UserService{
    @Override
    public void add() {
        System.out.println("增加user");
    }

    @Override
    public void delete() {
        System.out.println("删除user");
    }

    @Override
    public void update() {
        System.out.println("修改user");
    }

    @Override
    public void query() {
        System.out.println("查询user");
    }
}
