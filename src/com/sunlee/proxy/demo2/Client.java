package com.sunlee.proxy.demo2;

/**
 * @author sunlee
 */
public class Client {
    public static void main(String[] args) {
        UserServiceImpl userService = new UserServiceImpl();
        ProxyInvocationHandler proxy = new ProxyInvocationHandler();
        proxy.setTarget(userService);
        UserService proxyUserService = (UserService) proxy.getProxy();

        proxyUserService.add();
        proxyUserService.delete();
    }
}
