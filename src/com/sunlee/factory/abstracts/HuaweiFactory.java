package com.sunlee.factory.abstracts;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class HuaweiFactory implements ProductFactory{
    @Override
    public PhoneProduct getPhoneProduct() {
        return new HuaweiPhone();
    }

    @Override
    public RouterProduct getRouterProduct() {
        return new HuaweiRouter();
    }
}
