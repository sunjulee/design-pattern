package com.sunlee.factory.abstracts;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public interface PhoneProduct {
    void start();
    void shutdown();
    void callup();
    void sendSMS();
}
