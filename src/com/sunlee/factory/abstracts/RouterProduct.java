package com.sunlee.factory.abstracts;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public interface RouterProduct {
    void start();
    void shutdown();
    void openWifi();
    void setting();
}
