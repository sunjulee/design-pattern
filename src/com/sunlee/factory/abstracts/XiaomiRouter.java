package com.sunlee.factory.abstracts;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class XiaomiRouter implements RouterProduct {
    @Override
    public void start() {
        System.out.println("开启小米路由器！");
    }

    @Override
    public void shutdown() {
        System.out.println("关闭小米路由器！");
    }

    @Override
    public void openWifi() {
        System.out.println("连接小米路由器！");
    }

    @Override
    public void setting() {
        System.out.println("设置小米路由器！");
    }
}
