package com.sunlee.factory.abstracts;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class HuaweiRouter implements RouterProduct {
    @Override
    public void start() {
        System.out.println("开启华为路由器！");
    }

    @Override
    public void shutdown() {
        System.out.println("关闭华为路由器！");
    }

    @Override
    public void openWifi() {
        System.out.println("连接华为路由器！");
    }

    @Override
    public void setting() {
        System.out.println("设置华为路由器！");
    }
}
