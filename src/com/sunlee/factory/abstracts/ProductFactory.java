package com.sunlee.factory.abstracts;

/**
 * 抽象产品工厂
 *
 * @Author sunjulei
 * @Date 2021/10/17
 */
public interface ProductFactory {

    // 生产手机
    PhoneProduct getPhoneProduct();

    // 生产路由器
    RouterProduct getRouterProduct();
}
