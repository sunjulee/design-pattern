package com.sunlee.factory.abstracts;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class XiaomiFactory implements ProductFactory{
    @Override
    public PhoneProduct getPhoneProduct() {
        return new XiaomiPhone();
    }

    @Override
    public RouterProduct getRouterProduct() {
        return new XiaomiRouter();
    }
}
