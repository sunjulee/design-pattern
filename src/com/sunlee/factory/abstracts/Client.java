package com.sunlee.factory.abstracts;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class Client {
    public static void main(String[] args) {

        System.out.println("===========小米系列产品===============");
        XiaomiFactory xiaomiFactory = new XiaomiFactory();
        PhoneProduct xiaomiPhone = xiaomiFactory.getPhoneProduct();
        xiaomiPhone.start();
        xiaomiPhone.shutdown();
        xiaomiPhone.callup();
        xiaomiPhone.sendSMS();
        RouterProduct xiaomiRouter = xiaomiFactory.getRouterProduct();
        xiaomiRouter.start();
        xiaomiRouter.shutdown();
        xiaomiRouter.openWifi();
        xiaomiRouter.setting();

        System.out.println("===========华为系列产品===============");
        HuaweiFactory huaweiFactory = new HuaweiFactory();
        PhoneProduct huaweiPhone = huaweiFactory.getPhoneProduct();
        huaweiPhone.start();
        huaweiPhone.shutdown();
        huaweiPhone.callup();
        huaweiPhone.sendSMS();
        RouterProduct huaweiRouter = huaweiFactory.getRouterProduct();
        huaweiRouter.start();
        huaweiRouter.shutdown();
        huaweiRouter.openWifi();
        huaweiRouter.setting();
    }
}
