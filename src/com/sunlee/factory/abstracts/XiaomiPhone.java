package com.sunlee.factory.abstracts;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class XiaomiPhone implements PhoneProduct{
    @Override
    public void start() {
        System.out.println("开启小米手机！");
    }

    @Override
    public void shutdown() {
        System.out.println("关闭小米手机！");
    }

    @Override
    public void callup() {
        System.out.println("小米手机打电话！");
    }

    @Override
    public void sendSMS() {
        System.out.println("小米手机发短信！");
    }
}
