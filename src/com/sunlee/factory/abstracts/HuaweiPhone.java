package com.sunlee.factory.abstracts;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class HuaweiPhone implements PhoneProduct{
    @Override
    public void start() {
        System.out.println("开启华为手机！");
    }

    @Override
    public void shutdown() {
        System.out.println("关闭华为手机！");
    }

    @Override
    public void callup() {
        System.out.println("华为手机打电话！");
    }

    @Override
    public void sendSMS() {
        System.out.println("华为手机发短信！");
    }
}
