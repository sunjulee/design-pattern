package com.sunlee.factory.method;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class BaoMaFactory implements CarFactory{
    @Override
    public Car getCar() {
        return new BaoMa();
    }
}
