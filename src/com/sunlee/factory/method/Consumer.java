package com.sunlee.factory.method;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class Consumer {
    public static void main(String[] args) {
        Car car1 = new TeslaFactory().getCar();
        Car car2 = new WuLinFactory().getCar();
        Car car3 = new BaoMaFactory().getCar();
        car1.name();
        car2.name();
        car3.name();
    }
}
