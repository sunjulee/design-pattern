package com.sunlee.factory.method;

/**
 * 工厂方法模式
 *
 * @Author sunjulei
 * @Date 2021/10/17
 */
public interface CarFactory {
    Car getCar();
}
