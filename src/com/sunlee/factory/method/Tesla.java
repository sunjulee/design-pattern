package com.sunlee.factory.method;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class Tesla implements Car {
    @Override
    public void name() {
        System.out.println("特斯拉！！");
    }
}
