package com.sunlee.factory.method;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class TeslaFactory implements CarFactory{
    @Override
    public Car getCar() {
        return new Tesla();
    }
}
