package com.sunlee.factory.method;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public interface Car {
    void name();
}
