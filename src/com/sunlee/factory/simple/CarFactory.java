package com.sunlee.factory.simple;

/**
 * 简单工厂模式/静态工厂模式，但违背了开闭原则
 *
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class CarFactory {
    // 方法一
    public static Car getCar(String name) {
        if ("五菱".equals(name)) {
            return new WuLin();
        } else if ("特斯拉".equals(name)) {
            return new Tesla();
        }

        return null;
    }

    // 方法二
    public static Car getWuLin(){
        return new WuLin();
    }
    public static Car getTesla(){
        return new Tesla();
    }
}
