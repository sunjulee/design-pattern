package com.sunlee.factory.simple;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public interface Car {
    void name();
}
