package com.sunlee.factory.simple;

/**
 * @Author sunjulei
 * @Date 2021/10/17
 */
public class Consumer {
    public static void main(String[] args) {
        // Car car1 = new Tesla();
        // Car car2 = new WuLin();

        // Car car1 = CarFactory.getCar("五菱");
        // Car car2 = CarFactory.getCar("特斯拉");

        Car car1 = CarFactory.getWuLin();
        Car car2 = CarFactory.getTesla();
        car1.name();
        car2.name();
    }
}
