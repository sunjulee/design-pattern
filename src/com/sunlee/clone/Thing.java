package com.sunlee.clone;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 1、实现Cloneable接口的为桥拷贝，可对每个对象进行浅拷贝，从而实现深拷贝
 * 2、注意：浅拷贝时是不走构造函数的，因为是从堆内存中以二进制流方式进行拷贝，重新分配一块内存
 * 3、实现深拷贝可通过实现Serializable接口来完成
 */
public class Thing implements Cloneable {
    //定义一个私有变量
    private ArrayList<String> arrayList = new ArrayList<String>();

    @Override
    public Thing clone() {
        Thing thing = null;
        try {
            thing = (Thing) super.clone();
            thing.arrayList = (ArrayList<String>) this.arrayList.clone();//（深拷贝）
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return thing;
    }


    //设置HashMap的值
    public void setValue(String value){
        this.arrayList.add(value);
    }
    //取得arrayList的值
    public ArrayList<String> getValue(){
        return this.arrayList;
    }
}